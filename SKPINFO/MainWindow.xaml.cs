﻿using System;
using System.Windows;
using System.Windows.Input;
using static SKPINFO.SkpApi;
namespace SKPINFO
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        //关闭程序
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Go_Click(object sender, RoutedEventArgs e)
        {
            Info.Text = Skpinfo(Password.Password);
        }

        //窗口鼠标拖动
        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}
